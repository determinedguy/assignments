package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }

}

class SistemAkademikGUI extends JFrame{
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontList = new Font("Century Gothic", Font.BOLD , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);

    public SistemAkademikGUI(){

        // Membuat Frame
        // Add the title to the title bar
        JFrame frame = new JFrame("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);

        // Call HomeGUI for the welcome screen
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);

    }

    // Method for generating elements and sorting combo boxes
    public static JButton buttonMaker(String name) {
        JButton button = new JButton(name);
        button.setFont(SistemAkademikGUI.fontGeneral);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setBackground(Color.white);
        button.setOpaque(true);
        return button;
    }

    public static JLabel titleMaker(String title) {
        JLabel titleLabel = new JLabel();
        titleLabel.setText(title);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        return titleLabel;
    }

    public static JLabel textMaker(String text) {
        JLabel textLabel = new JLabel();
        textLabel.setText(text);
        textLabel.setFont(SistemAkademikGUI.fontGeneral);
        textLabel.setForeground(Color.white);
        textLabel.setHorizontalAlignment(JLabel.CENTER);
        textLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        return textLabel;
    }

    public static JLabel listMaker(String text) {
        JLabel listLabel = new JLabel();
        listLabel.setText(text);
        listLabel.setFont(SistemAkademikGUI.fontList);
        listLabel.setForeground(Color.white);
        listLabel.setHorizontalAlignment(JLabel.CENTER);
        listLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        return listLabel;
    }

    public static JTextField textFieldMaker() {
        JTextField textField = new JTextField();
        textField.setMaximumSize(new Dimension(250, 20));
        textField.setAlignmentX(Component.CENTER_ALIGNMENT);
        return textField;
    }

    public static JComboBox comboBoxMaker() {
        JComboBox comboBox = new JComboBox();
        comboBox.setMaximumSize(new Dimension(250, 20));
        comboBox.setFont(SistemAkademikGUI.fontGeneral);
        // Set five first items as the first shown one, scroll if want to see else
        comboBox.setMaximumRowCount(5);
        return comboBox;
    }

    public static void updateComboBoxMahasiswa(ArrayList<Mahasiswa> daftarMahasiswa, JComboBox npmComboBox) {
        // Sort the student list manually (by bubble sorting), store them in arrays
        Mahasiswa[] daftarMahasiswaSorted = daftarMahasiswa.toArray(new Mahasiswa[0]);
        int n = daftarMahasiswaSorted.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-i-1; j++) {
                if (daftarMahasiswaSorted[j].getNpm() > daftarMahasiswaSorted[j+1].getNpm()) {
                    Mahasiswa temp = daftarMahasiswaSorted[j];
                    daftarMahasiswaSorted[j] = daftarMahasiswaSorted[j+1];
                    daftarMahasiswaSorted[j+1] = temp;
                }
            }
        }

        // Remove the added items (reset the combo box)
        npmComboBox.removeAllItems();

        // Add the sorted items to the combo box
        for (Mahasiswa mahasiswa : daftarMahasiswaSorted) {
            npmComboBox.addItem(mahasiswa);
        }
    }

    public static void updateComboBoxMatkul(ArrayList<MataKuliah> daftarMataKuliah, JComboBox namaMatkulComboBox) {
        // Sort the subject list manually (by bubble sorting), store them in arrays
        MataKuliah[] daftarMataKuliahSorted = daftarMataKuliah.toArray(new MataKuliah[0]);
        int m = daftarMataKuliahSorted.length;
        for (int i = 0; i < m-1; i++) {
            for (int j = 0; j < m-i-1; j++) {
                if (daftarMataKuliahSorted[j].getNama().compareTo(daftarMataKuliahSorted[j+1].getNama()) > 0) {
                    MataKuliah temp = daftarMataKuliahSorted[j];
                    daftarMataKuliahSorted[j] = daftarMataKuliahSorted[j+1];
                    daftarMataKuliahSorted[j+1] = temp;
                }
            }
        }

        // Remove the added items (reset the combo box)
        namaMatkulComboBox.removeAllItems();

        // Add the sorted items to the combo box
        for (MataKuliah mataKuliah : daftarMataKuliahSorted) {
            namaMatkulComboBox.addItem(mataKuliah);
        }
    }

}
