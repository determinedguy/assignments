package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI extends JPanel {

    private JComboBox npmComboBox;
    private JComboBox namaMatkulComboBox;
    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Fetch data for future purposes
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Tambah IRS");
        JLabel npmLabel = SistemAkademikGUI.textMaker("Pilih NPM");
        JLabel namaMatkulLabel = SistemAkademikGUI.textMaker("Pilih Nama Matkul");
        this.npmComboBox = SistemAkademikGUI.comboBoxMaker();
        this.namaMatkulComboBox = SistemAkademikGUI.comboBoxMaker();
        JButton tambahkanButton = SistemAkademikGUI.buttonMaker("Tambahkan");
        JButton kembaliButton = SistemAkademikGUI.buttonMaker("Kembali");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmComboBox);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaMatkulLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaMatkulComboBox);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(tambahkanButton);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kembaliButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        tambahkanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Get the data details
                Mahasiswa mahasiswa = (Mahasiswa) npmComboBox.getSelectedItem();
                MataKuliah mataKuliah = (MataKuliah) namaMatkulComboBox.getSelectedItem();

                // Initialize message variable
                String message = "";

                // Check for conditions
                // If one of the fields is empty, return exclamation message
                // Else, proceed and return success message (if it's succeed)
                if (mahasiswa == null || mataKuliah == null) {
                    message = "Mohon isi seluruh field";
                } else {
                    message = mahasiswa.addMatkul(mataKuliah);
                }

                // Print the message on a message dialog
                JOptionPane.showMessageDialog(frame, message);
            }
        });

        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });

    }

    public void updateComboBox(ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah) {
        // Sort the student and the subject list by calling methods
        SistemAkademikGUI.updateComboBoxMahasiswa(daftarMahasiswa, npmComboBox);
        SistemAkademikGUI.updateComboBoxMatkul(daftarMataKuliah, namaMatkulComboBox);
    }

}
