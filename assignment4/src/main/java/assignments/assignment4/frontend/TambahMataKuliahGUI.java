package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI extends JPanel {

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Tambah Mata Kuliah");
        JLabel kodeMatkulLabel = SistemAkademikGUI.textMaker("Kode Mata Kuliah:");
        JLabel namaMatkulLabel = SistemAkademikGUI.textMaker("Nama Mata Kuliah:");
        JLabel sksLabel = SistemAkademikGUI.textMaker("SKS:");
        JLabel kapasitasLabel = SistemAkademikGUI.textMaker("Kapasitas:");
        JTextField kodeMatkulField = SistemAkademikGUI.textFieldMaker();
        JTextField namaMatkulField = SistemAkademikGUI.textFieldMaker();
        JTextField sksField = SistemAkademikGUI.textFieldMaker();
        JTextField kapasitasField = SistemAkademikGUI.textFieldMaker();
        JButton tambahkanButton = SistemAkademikGUI.buttonMaker("Tambahkan");
        JButton kembaliButton = SistemAkademikGUI.buttonMaker("Kembali");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kodeMatkulLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kodeMatkulField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaMatkulLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaMatkulField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(sksLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(sksField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kapasitasLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kapasitasField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(tambahkanButton);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kembaliButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        tambahkanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Get the data details
                String kodeMatkul = kodeMatkulField.getText().trim();
                String namaMatkul = namaMatkulField.getText().trim();
                String sks = sksField.getText().trim();
                String kapasitas = kapasitasField.getText().trim();

                // Initialize message variable and clearance field status
                String message = "";
                boolean status = false;

                // Check for conditions
                // If one of the fields is empty, return exclamation message
                // Else if the subject is already inputted, return exclamation message and clear the text field
                // Else, proceed and return success message and clear the text field
                if (kodeMatkul.equals("")  || namaMatkul.equals("") || sks.equals("") || kapasitas.equals("")) {
                    message = "Mohon isi seluruh field";
                } else if (cekNamaMatkul(daftarMataKuliah, namaMatkul)) {
                    message = String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", namaMatkul);
                    status = true;
                } else {
                    daftarMataKuliah.add(new MataKuliah(kodeMatkul, namaMatkul, Integer.parseInt(sks), Integer.parseInt(kapasitas)));
                    message = String.format("Mata Kuliah %s berhasil ditambahkan", namaMatkul);
                    status = true;
                }

                // If the clearance status is true (means the text field should be cleared), clear the text field
                if (status) {
                    kodeMatkulField.setText("");
                    namaMatkulField.setText("");
                    sksField.setText("");
                    kapasitasField.setText("");
                }

                // Print the message on a message dialog
                JOptionPane.showMessageDialog(frame, message);
            }
        });

        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });

    }

    private boolean cekNamaMatkul(ArrayList<MataKuliah> daftarMataKuliah, String namaMatkul) {
        // Check if the subject (by name) is available, return true if it's available, return false if vice versa
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(namaMatkul)) {
                return true;
            }
        }
        return false;
    }
    
}
