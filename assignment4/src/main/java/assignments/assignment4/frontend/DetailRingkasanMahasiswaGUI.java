package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI extends JPanel {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Call cekIRS method from Mahasiswa to check problems
        mahasiswa.cekIRS();

        // Fetch the data from the student
        String nama = mahasiswa.getNama();
        long npm = mahasiswa.getNpm();
        String jurusan = mahasiswa.getJurusan();
        int totalSKS = mahasiswa.getTotalSKS();
        MataKuliah[] daftarMatkul = mahasiswa.getMataKuliah();
        int banyakMatkul = mahasiswa.getBanyakMatkul();
        String[] daftarMasalahIRS = mahasiswa.getMasalahIRS();
        int banyakMasalahIRS = mahasiswa.getBanyakMasalahIRS();

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Detail Ringkasan Mahasiswa");
        JLabel namaLabel = SistemAkademikGUI.textMaker("Nama: " + nama);
        JLabel npmLabel = SistemAkademikGUI.textMaker("NPM: " + npm);
        JLabel jurusanLabel = SistemAkademikGUI.textMaker("Jurusan: " + jurusan);
        JLabel matkulLabel = SistemAkademikGUI.textMaker("Daftar Mata Kuliah:");
        JLabel totalSksLabel = SistemAkademikGUI.textMaker("Total SKS: " + totalSKS);
        JLabel cekIRSLabel = SistemAkademikGUI.textMaker("Hasil Pengecekan IRS:");
        JButton selesaiButton = SistemAkademikGUI.buttonMaker("Selesai");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(jurusanLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(matkulLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        // Check if the amount of subject taken is zero, print special message if condition fulfilled
        // Else, iterate the subject list and add each of it
        if (banyakMatkul == 0) {
            JLabel daftarMatkulLabel = SistemAkademikGUI.listMaker("Belum ada mata kuliah yang diambil.");
            this.add(daftarMatkulLabel);
            this.add(Box.createRigidArea(new Dimension(0, 10)));
        } else {
            for (int i = 0;i < banyakMatkul;i++) {
                MataKuliah mataKuliah = daftarMatkul[i];
                JLabel daftarMatkulLabel = SistemAkademikGUI.listMaker((i+1) + ". " + mataKuliah.getNama());
                this.add(daftarMatkulLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
            }
        }

        this.add(totalSksLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(cekIRSLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        // Check if the amount of problem occurred is zero, print special message if condition fulfilled
        // Else, iterate the problem list and add each of it
        if (banyakMasalahIRS == 0) {
            JLabel daftarMasalahIRSLabel = SistemAkademikGUI.listMaker("IRS tidak bermasalah.");
            this.add(daftarMasalahIRSLabel);
            this.add(Box.createRigidArea(new Dimension(0, 10)));
        } else {
            for (int i = 0;i < banyakMatkul;i++) {
                JLabel daftarMasalahIRSLabel = SistemAkademikGUI.listMaker((i+1) + ". " + daftarMasalahIRS[i]);
                this.add(daftarMasalahIRSLabel);
                this.add(Box.createRigidArea(new Dimension(0, 10)));
            }
        }

        this.add(selesaiButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        selesaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });
        
    }
}
