package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI extends JPanel {

    private JComboBox npmComboBox;
    private ArrayList<Mahasiswa> daftarMahasiswa;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Fetch data for future purposes
        this.daftarMahasiswa = daftarMahasiswa;

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Ringkasan Mahasiswa");
        JLabel npmLabel = SistemAkademikGUI.textMaker("Pilih NPM");
        this.npmComboBox = SistemAkademikGUI.comboBoxMaker();
        JButton lihatButton = SistemAkademikGUI.buttonMaker("Lihat");
        JButton kembaliButton = SistemAkademikGUI.buttonMaker("Kembali");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmComboBox);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(lihatButton);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kembaliButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        lihatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Get the data details
                Mahasiswa mahasiswa = (Mahasiswa) npmComboBox.getSelectedItem();

                // Initialize message variable
                String message = "";

                // Check for conditions
                // If the field is empty, return exclamation message
                // Else, proceed and pass it to DetailRingkasanMahasiswaGUI
                if (mahasiswa == null) {
                    message = "Mohon isi seluruh field";
                    // Print the message on a message dialog
                    JOptionPane.showMessageDialog(frame, message);
                } else {
                    // Pass the student object to DetailRingkasanMahasiswaGUI and show the DetailRingkasanMahasiswaGUI
                    Container page = frame.getContentPane();
                    CardLayout cardLayout = (CardLayout) page.getLayout();
                    DetailRingkasanMahasiswaGUI detailMahasiswaPanel = new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    detailMahasiswaPanel.setBackground(Color.darkGray);
                    // Set a scrollable panel for the detail panel
                    JScrollPane scrollMahasiswaPanel = new JScrollPane(detailMahasiswaPanel);
                    scrollMahasiswaPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                    scrollMahasiswaPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                    scrollMahasiswaPanel.getVerticalScrollBar().setBackground(Color.white);
                    // Add the scrollable panel and show it
                    page.add(scrollMahasiswaPanel);
                    cardLayout.addLayoutComponent(scrollMahasiswaPanel, "DetailRingkasanMahasiswa");
                    cardLayout.show(page, "DetailRingkasanMahasiswa");
                }
            }
        });

        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });

    }

    public void updateComboBox(ArrayList<Mahasiswa> daftarMahasiswa) {
        // Sort the student list by calling method
        SistemAkademikGUI.updateComboBoxMahasiswa(daftarMahasiswa, npmComboBox);
    }

}
