package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI extends JPanel {

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Tambah Mahasiswa");
        JLabel namaLabel = SistemAkademikGUI.textMaker("Nama:");
        JLabel npmLabel = SistemAkademikGUI.textMaker("NPM:");
        JTextField namaField = SistemAkademikGUI.textFieldMaker();
        JTextField npmField = SistemAkademikGUI.textFieldMaker();
        JButton tambahkanButton = SistemAkademikGUI.buttonMaker("Tambahkan");
        JButton kembaliButton = SistemAkademikGUI.buttonMaker("Kembali");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(npmField);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(tambahkanButton);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kembaliButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        tambahkanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Get the data details
                String nama = namaField.getText().trim();
                String npmString = npmField.getText().trim();

                // Initialize message variable and clearance field status
                String message = "";
                boolean status = false;

                // Check for conditions
                // If one of the fields is empty, return exclamation message
                // Else if the NPM is already inputted, return exclamation message and clear the text field
                // Else, proceed and return success message and clear the text field
                if (nama.equals("") || npmString.equals("")) {
                    message = "Mohon isi seluruh field";
                } else if (cekNpmMahasiswa(daftarMahasiswa, npmString)) {
                    message = String.format("NPM %s sudah pernah ditambahkan sebelumnya", npmString);
                    status = true;
                } else {
                    daftarMahasiswa.add(new Mahasiswa(nama, Long.parseLong(npmString)));
                    message = String.format("Mahasiswa %s-%s berhasil ditambahkan", nama, npmString);
                    status = true;
                }

                // If the clearance status is true (means the text field should be cleared), clear the text field
                if (status) {
                    namaField.setText("");
                    npmField.setText("");
                }

                // Print the message on a message dialog
                JOptionPane.showMessageDialog(frame, message);
            }
        });

        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });

    }

    private boolean cekNpmMahasiswa(ArrayList<Mahasiswa> daftarMahasiswa, String npm) {
        // Check if the NPM is available, return true if it's available, return false if vice versa
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == Long.parseLong(npm)) {
                return true;
            }
        }
        return false;
    }
    
}
