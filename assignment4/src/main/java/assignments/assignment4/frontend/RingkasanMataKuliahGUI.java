package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI extends JPanel {

    private JComboBox namaMatkulComboBox;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Fetch data for future purposes
        this.daftarMataKuliah = daftarMataKuliah;

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Ringkasan Mata Kuliah");
        JLabel matkulLabel = SistemAkademikGUI.textMaker("Pilih Nama Matkul");
        this.namaMatkulComboBox = SistemAkademikGUI.comboBoxMaker();
        JButton lihatButton = SistemAkademikGUI.buttonMaker("Lihat");
        JButton kembaliButton = SistemAkademikGUI.buttonMaker("Kembali");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(matkulLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaMatkulComboBox);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(lihatButton);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kembaliButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        lihatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Get the data details
                MataKuliah mataKuliah = (MataKuliah) namaMatkulComboBox.getSelectedItem();

                // Initialize message variable
                String message = "";

                // Check for conditions
                // If the field is empty, return exclamation message
                // Else, proceed and pass it to DetailRingkasanMahasiswaGUI
                if (mataKuliah == null) {
                    message = "Mohon isi seluruh field";
                    // Print the message on a message dialog
                    JOptionPane.showMessageDialog(frame, message);
                } else {
                    // Pass the student object to DetailRingkasanMataKuliahGUI and show DetailRingkasanMataKuliahGUI
                    Container page = frame.getContentPane();
                    CardLayout cardLayout = (CardLayout) page.getLayout();
                    DetailRingkasanMataKuliahGUI detailMatkulPanel = new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                    detailMatkulPanel.setBackground(Color.darkGray);
                    // Set a scrollable panel for the detail panel
                    JScrollPane scrollMatkulPanel = new JScrollPane(detailMatkulPanel);
                    scrollMatkulPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                    scrollMatkulPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                    scrollMatkulPanel.getVerticalScrollBar().setBackground(Color.white);
                    // Add the scrollable panel and show it
                    page.add(scrollMatkulPanel);
                    cardLayout.addLayoutComponent(scrollMatkulPanel, "DetailRingkasanMatkul");
                    cardLayout.show(page, "DetailRingkasanMatkul");
                }
            }
        });
        kembaliButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });
        
    }

    public void updateComboBox(ArrayList<MataKuliah> daftarMataKuliah) {
        // Sort the subject list by calling method
        SistemAkademikGUI.updateComboBoxMatkul(daftarMataKuliah, namaMatkulComboBox);
    }

}
