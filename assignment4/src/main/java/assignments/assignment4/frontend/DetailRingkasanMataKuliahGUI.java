package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI extends JPanel {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);

        // Fetch the data from the student
        String nama = mataKuliah.getNama();
        String kode = mataKuliah.getKode();
        int sks = mataKuliah.getSKS();
        int jumlahMahasiswa = mataKuliah.getJumlahMahasiswa();
        int kapasitas = mataKuliah.getKapasitas();
        Mahasiswa[] daftarMahasiswaMatkul = mataKuliah.getDaftarMahasiswa();

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Detail Ringkasan Mata Kuliah");
        JLabel namaLabel = SistemAkademikGUI.textMaker("Nama mata kuliah: " + nama);
        JLabel kodeLabel = SistemAkademikGUI.textMaker("Kode: " + kode);
        JLabel sksLabel = SistemAkademikGUI.textMaker("SKS: " + sks);
        JLabel jumlahMahasiswaLabel = SistemAkademikGUI.textMaker("Jumlah mahasiswa: " + jumlahMahasiswa);
        JLabel kapasitasLabel = SistemAkademikGUI.textMaker("Kapasitas: " + kapasitas);
        JLabel mahasiswaLabel = SistemAkademikGUI.textMaker("Daftar Mahasiswa:");
        JButton selesaiButton = SistemAkademikGUI.buttonMaker("Selesai");

        // Add the elements to the panel (with the spaces)
        this.add(Box.createVerticalGlue());
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(namaLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kodeLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(sksLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(jumlahMahasiswaLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(kapasitasLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(mahasiswaLabel);
        this.add(Box.createRigidArea(new Dimension(0, 10)));

        // Check if the amount of student enrolled is zero, print special message if condition fulfilled
        // Else, iterate the student list and add each of it
        if (jumlahMahasiswa == 0) {
            JLabel daftarMahasiswaLabel = SistemAkademikGUI.listMaker("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            this.add(daftarMahasiswaLabel);
        } else {
            for (int i = 0; i < jumlahMahasiswa; i++) {
                Mahasiswa mahasiswa = daftarMahasiswaMatkul[i];
                JLabel daftarMahasiswaLabel = SistemAkademikGUI.listMaker((i + 1) + ". " + mahasiswa.getNama());
                this.add(daftarMahasiswaLabel);
            }
        }

        // Add the remaining elements to the panel (with the spaces)
        this.add(Box.createRigidArea(new Dimension(0, 10)));
        this.add(selesaiButton);
        this.add(Box.createVerticalGlue());

        // Add the desired behavior to each buttons
        selesaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go back to HomeGUI panel
                Container page = frame.getContentPane();
                CardLayout cardLayout = (CardLayout) page.getLayout();
                cardLayout.show(page, "Home");
            }
        });
        
    }
}
