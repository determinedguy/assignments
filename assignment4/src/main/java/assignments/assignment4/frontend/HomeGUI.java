package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Initialize the homeGUI panel
        JPanel homePanel = new JPanel();
        homePanel.setBackground(Color.darkGray);

        // Use BoxLayout as the panel layout and add the layout to the panel
        BoxLayout boxLayout = new BoxLayout(homePanel, BoxLayout.Y_AXIS);
        homePanel.setLayout(boxLayout);

        // Initialize the elements
        JLabel titleLabel = SistemAkademikGUI.titleMaker("Selamat datang di Sistem Akademik");
        JButton tambahMahasiswaButton = SistemAkademikGUI.buttonMaker("Tambah Mahasiswa");
        JButton tambahMatkulButton = SistemAkademikGUI.buttonMaker("Tambah Mata Kuliah");
        JButton tambahIRSButton = SistemAkademikGUI.buttonMaker("Tambah IRS");
        JButton hapusIRSButton = SistemAkademikGUI.buttonMaker("Hapus IRS");
        JButton ringkasanMahasiswaButton = SistemAkademikGUI.buttonMaker("Lihat Ringkasan Mahasiswa");
        JButton ringkasanMatkulButton = SistemAkademikGUI.buttonMaker("Lihat Ringkasan Mata Kuliah");

        // Add the elements to the homePanel
        // Use vertical glue to center all of the elements
        // Source: https://stackoverflow.com/a/42848416/8487665
        homePanel.add(Box.createVerticalGlue());
        homePanel.add(titleLabel);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(tambahMahasiswaButton);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(tambahMatkulButton);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(tambahIRSButton);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(hapusIRSButton);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(ringkasanMahasiswaButton);
        homePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        homePanel.add(ringkasanMatkulButton);
        homePanel.add(Box.createVerticalGlue());

        // Make new panels for navigation to other pages (and add the background color)
        TambahMahasiswaGUI tambahMahasiswaPanel = new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
        tambahMahasiswaPanel.setBackground(Color.darkGray);
        TambahMataKuliahGUI tambahMatkulPanel = new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
        tambahMatkulPanel.setBackground(Color.darkGray);
        TambahIRSGUI tambahIRSPanel = new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
        tambahIRSPanel.setBackground(Color.darkGray);
        HapusIRSGUI hapusIRSPanel = new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
        hapusIRSPanel.setBackground(Color.darkGray);
        RingkasanMahasiswaGUI ringkasanMahasiswaPanel = new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
        ringkasanMahasiswaPanel.setBackground(Color.darkGray);
        RingkasanMataKuliahGUI ringkasanMatkulPanel = new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
        ringkasanMatkulPanel.setBackground(Color.darkGray);

        // Call the container page and set the layout (with CardLayout) for navigation between frames
        Container page = frame.getContentPane();
        CardLayout cardLayout = new CardLayout();
        page.setLayout(cardLayout);

        // Add panels to the container
        page.add(homePanel);
        page.add(tambahMahasiswaPanel);
        page.add(tambahMatkulPanel);
        page.add(tambahIRSPanel);
        page.add(hapusIRSPanel);
        page.add(ringkasanMahasiswaPanel);
        page.add(ringkasanMatkulPanel);

        // Add component to cardLayout
        cardLayout.addLayoutComponent(homePanel, "Home");
        cardLayout.addLayoutComponent(tambahMahasiswaPanel, "TambahMahasiswa");
        cardLayout.addLayoutComponent(tambahMatkulPanel, "TambahMatkul");
        cardLayout.addLayoutComponent(tambahIRSPanel, "TambahIRS");
        cardLayout.addLayoutComponent(hapusIRSPanel, "HapusIRS");
        cardLayout.addLayoutComponent(ringkasanMahasiswaPanel, "RingkasanMahasiswa");
        cardLayout.addLayoutComponent(ringkasanMatkulPanel, "RingkasanMatkul");

        // Show the home page as the first page
        cardLayout.show(page, "Home");

        // Add the desired behavior to each buttons
        tambahMahasiswaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go to TambahMahasiswaGUI panel
                cardLayout.show(page, "TambahMahasiswa");
            }
        });

        tambahMatkulButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Go to TambahMataKuliahGUI panel
                cardLayout.show(page, "TambahMatkul");
            }
        });

        tambahIRSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Call updateComboBox method to update the value of the combo boxes
                tambahIRSPanel.updateComboBox(daftarMahasiswa, daftarMataKuliah);
                // Go to TambahIRSGUI panel
                cardLayout.show(page, "TambahIRS");
            }
        });

        hapusIRSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Call updateComboBox method to update the value of the combo boxes
                hapusIRSPanel.updateComboBox(daftarMahasiswa, daftarMataKuliah);
                // Go to HapusIRSGUI panel
                cardLayout.show(page, "HapusIRS");
            }
        });

        ringkasanMahasiswaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Call updateComboBox method to update the value of the combo box
                ringkasanMahasiswaPanel.updateComboBox(daftarMahasiswa);
                // Go to RingkasanMahasiswaGUI panel
                cardLayout.show(page, "RingkasanMahasiswa");
            }
        });

        ringkasanMatkulButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // Call updateComboBox method to update the value of the combo box
                ringkasanMatkulPanel.updateComboBox(daftarMataKuliah);
                // Go to RingkasanMataKuliahGUI panel
                cardLayout.show(page, "RingkasanMatkul");
            }
        });

    }

}
