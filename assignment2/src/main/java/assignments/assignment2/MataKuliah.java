package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public String toString() {
        return nama;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public int getSKS() {
        return sks;
    }

    public String getKode() {
        return kode;
    }

    public int getJumlahMahasiswa() {
        return jumlahMahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Check the last empty position of daftarMahasiswa and add the student to the position
        this.daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
        jumlahMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // Make a temporary array which contains all of remaining students
        Mahasiswa[] tempArr = new Mahasiswa[kapasitas];
        int count = 0;
        for (Mahasiswa tempMahasiswa : daftarMahasiswa) {
            if (!(tempMahasiswa == mahasiswa)) {
                tempArr[count] = tempMahasiswa;
                count++;
            }
        }
        //  Reference the new array to daftarMahasiswa
        this.daftarMahasiswa = tempArr;
        // Decrease the amount of students enrolling the course
        jumlahMahasiswa--;
    }

}
