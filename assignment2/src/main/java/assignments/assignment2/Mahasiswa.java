package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int jumlahMataKuliah;

    public Mahasiswa(String nama, long npm) {
        this.nama = nama;
        this.npm = npm;
        // Get the major code from NPM and assign the private variable according to the major code
        String temp = String.valueOf(npm).substring(2, 4);
        if (temp.equals("01")) this.jurusan = "Ilmu Komputer";
        else this.jurusan = "Sistem Informasi";
    }

    public String toString() {
        return nama;
    }

    public long getNPM() {
        return npm;
    }

    public String getJurusan() {
        return jurusan;
    }

    public int getTotalSKS() {
        return totalSKS;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }

    public int getJumlahMataKuliah() {
        return jumlahMataKuliah;
    }

    public boolean cekStatusMataKuliah(MataKuliah mataKuliah) {
        // Check the list of subjects taken by looping the list, return true if the subject is in the list
        for (int i = 0;i < jumlahMataKuliah;i++) {
            if (daftarMataKuliah[i].equals(mataKuliah)) {
                return true;
            }
        }
        // Return false if the subject is not in the list or there is no subject added
        return false;
    }

    public void addMatkul(MataKuliah mataKuliah){
        // STEP 1: Check if the subject is taken already, print error status if the student took the subject already
        if (cekStatusMataKuliah(mataKuliah)) {
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya.\n", mataKuliah.toString());
        }
        // STEP 2: Check if the subject is fully taken, print error status if the subject is fully filled
        else if (mataKuliah.getJumlahMahasiswa() >= mataKuliah.getKapasitas()) {
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah.toString());
        }
        // STEP 3: Check if the student takes 10 subjects, print error status if the student takes 10 subjects already
        else if (jumlahMataKuliah >= 10) {
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
        }
        // If there is no error, add the subject
        else {
            // Get the last empty position on daftarMataKuliah and add the subject
            this.daftarMataKuliah[jumlahMataKuliah] = mataKuliah;
            this.jumlahMataKuliah++;
            // Add the SKS to totalSKS
            this.totalSKS += mataKuliah.getSKS();
            // Add the student to the subject, too
            mataKuliah.addMahasiswa(this);
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        // Check if the subject is not taken already, print error status if the student hasn't taken the subject
        if (!(cekStatusMataKuliah(mataKuliah))) {
            System.out.printf("[DITOLAK] %s belum pernah diambil.\n", mataKuliah.toString());
        }
        // If there is no error, drop the subject
        else {
            // Make a temporary array which contains all of remaining subjects
            MataKuliah[] tempArr = new MataKuliah[10];
            int count = 0;
            for (MataKuliah tempMatkul : daftarMataKuliah) {
                if (!(tempMatkul == mataKuliah)) {
                    tempArr[count] = tempMatkul;
                    count++;
                }
            }
            //  Reference the new array to daftarMataKuliah
            this.daftarMataKuliah = tempArr;
            // Subtract totalSKS by the SKS
            this.totalSKS -= mataKuliah.getSKS();
            // Decrease the amount of courses enrolled
            this.jumlahMataKuliah--;
            // Drop the student from the subject, too
            mataKuliah.dropMahasiswa(this);
        }
    }

    public void cekIRS(){
        // Initialize temporary variable to ease the checking of the major and subject
        String kodeJurusan;
        if (jurusan.equals("Ilmu Komputer")) kodeJurusan = "IK";
        else kodeJurusan = "SI";
        // Initialize masalahIRS (maximum problems < 20)
        masalahIRS = new String[20];
        int index = 0;
        // If totalSKS is bigger than 24, add error message to masalahIRS
        if (totalSKS > 24) {
            // Add the error message to masalahIRS
            masalahIRS[index] = "SKS yang Anda ambil lebih dari 24";
            // Increase the index
            index++;
        }
        // Check each subject whether it's conflicting with the student's major or not
        for (int i = 0;i < jumlahMataKuliah;i++) {
            MataKuliah loopMatkul = daftarMataKuliah[i];
            // Check if the major and the subject is conflicting (excluding "CS")
            if (!(loopMatkul.getKode().equals(kodeJurusan))) {
                if (!(loopMatkul.getKode().equals("CS"))) {
                    // Add the error message to masalahIRS
                    masalahIRS[index] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", loopMatkul.toString(), kodeJurusan);
                    // Increase the index
                    index++;
                }
            }
        }
        // If there is no error messages, print the message
        if (index == 0) {
            System.out.println("IRS tidak bermasalah.");
        }
        // Else, iterate each error and print with string formatting
        else {
            for (int i = 0;i < index;i++) {
                System.out.printf("%d. %s\n", i+1, masalahIRS[i]);
            }
        }
    }

}
