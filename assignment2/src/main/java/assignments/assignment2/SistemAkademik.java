package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        // Loop the daftarMahasiswa and search for the student by their NPM
        Mahasiswa tempMahasiswa = null;
        for (Mahasiswa loopMahasiswa : daftarMahasiswa) {
            if (loopMahasiswa.getNPM() == npm) {
                // Get the student and reference it to tempMahasiswa (to be returned)
                tempMahasiswa = loopMahasiswa;
                break;
            }
        }
        return tempMahasiswa;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        // Loop the daftarMataKuliah and search for the subject
        MataKuliah tempMatkul = null;
        for (MataKuliah loopMatkul : daftarMataKuliah) {
            if (loopMatkul.toString().equals(namaMataKuliah)) {
                // Get the subject and reference it to tempMatkul (to be returned)
                tempMatkul = loopMatkul;
                break;
            }
        }
        return tempMatkul;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // Get the student in the form of Mahasiswa object
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for (int i = 0; i < banyakMatkul; i++) {
            System.out.print("Nama matakuliah " + i + 1 + " : ");
            String namaMataKuliah = input.nextLine();
            // Get the subject in the form of MataKuliah object
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            // Add the subject to the student (in object form) [Will be verified in Mahasiswa class]
            mahasiswa.addMatkul(mataKuliah);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // Get the student in the form of Mahasiswa object
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Check if the student haven't taken any subjects (cekJumlahMataKuliah = 0)
        if (mahasiswa.getJumlahMataKuliah() == 0) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }
        // If the student takes subject (at least 1), proceed
        else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for (int i = 0; i < banyakMatkul; i++) {
                System.out.print("Nama matakuliah " + i + 1 + " : ");
                String namaMataKuliah = input.nextLine();
                // Get the subject in the form of MataKuliah object
                MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
                // Add the subject to the student (in object form)
                mahasiswa.dropMatkul(mataKuliah);
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        // Get the student in the form of Mahasiswa object
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.toString());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        // Iterate each subject taken and print with string formatting
        // If the student takes no subject, print the message
        if (mahasiswa.getJumlahMataKuliah() > 0) {
            int num = 1;
            for (int i = 0;i < mahasiswa.getJumlahMataKuliah();i++) {
                MataKuliah tempMatkul = mahasiswa.getDaftarMataKuliah()[i];
                System.out.printf("%d. %s\n", num, tempMatkul.toString());
                num++;
            }
        }
        else {
            System.out.println("Belum ada mata kuliah yang diambil");
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        mahasiswa.cekIRS();
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();

        // Get the subject in the form of MataKuliah object
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.toString());
        System.out.println("Kode: " + mataKuliah.getKode());
        System.out.println("SKS: " + mataKuliah.getSKS());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        // If there is at least a student takes the subject, iterate each student and print with string formatting
        // Else, print the message
        if (mataKuliah.getJumlahMahasiswa() > 0) {
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                System.out.printf("%d. %s\n", i + 1, mataKuliah.getDaftarMahasiswa()[i].toString());
            }
        }
        else {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }

    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            // Add the new subject to the array as an object of MataKuliah
            // Reference: https://stackoverflow.com/a/10400435
            daftarMataKuliah[i] = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            // Add the new student to the array as an object of Mahasiswa
            daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }

}
