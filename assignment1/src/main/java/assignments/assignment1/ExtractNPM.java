package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    public static boolean validate(long npm) {
        // STEP 1: validate NPM length by dividing NPM with 1E+13 (1x10^13)
        if (npm / 1E+13 == 0 || npm / 1E+13 > 9) return false;
        // STEP 2: validate major code
        String major = majorExtract(npm);
        // If null, the major code is invalid
        if (major == null) return false;
        // STEP 3: validate birth date and age
        String birthDate = birthDateExtractValidate(npm);
        // If null, the birth date is invalid
        if (birthDate == null) return false;
        // STEP 4: validate NPM code; if it's false, it's invalid
        return codeValidation(npm);
    }

    public static String extract(long npm) {
        // STEP 1: Extract year entrance by taking first two digits and add it with 2000
        long year = (long) (npm / 1E+12) + 2000;
        String yearStr = "Tahun masuk: " + year;
        // STEP 2: Extract the major
        String major = "Jurusan: " + majorExtract(npm);
        // STEP 3: Extract the birth date
        String birthDate = "Tanggal Lahir: " + birthDateExtractValidate(npm);
        // STEP 4: Concatenate strings and add '\n'
        String output = yearStr + '\n' + major + '\n' + birthDate;
        // Return the final output
        return output;
    }

    public static String majorExtract(long npm) {
        // Initialize collection of valid major codes
        int[] majorCodesNum = {1, 2, 3, 11, 12};
        String[] majorCodesStr = {"Ilmu Komputer", "Sistem Informasi", "Teknologi Informasi", "Teknik Telekomunikasi", "Teknik Elektro"};
        // Initialize temporary variables with invalid-indicating value
        int codeIndex = -1;
        // Extract the major code by dividing NPM with 10^10 and mod it with 100
        int majorCode = (int) (npm / 1E+10 % 100);
        // Check the major code
        for (int i = 0;i < majorCodesNum.length;i++) {
            if (majorCodesNum[i] == majorCode)  codeIndex = i;
        }
        // If the major code is invalid, return null
        if (codeIndex == -1) return null;
        // If the major code is valid, return the major
        return majorCodesStr[codeIndex];
    }

    public static String birthDateExtractValidate(long npm) {
        // Initialize collection of months with 31 and 30 days (excluding February)
        int[] oddMonth = {1, 3, 5, 7, 8, 10, 12};
        int[] evenMonth = {4, 6, 9, 11};
        // Initialize temporary variables
        String birthDateStr = null;
        // Extract the birth date by dividing NPM with 100 and mod it with 1E+8
        long birthDateFull = (long) (npm / 100 % 1E+8);
        // Extract the year entrance by dividing NPM with 1E+12 and add it with 2000
        long yearEntrance = (long) (npm / 1E+12) + 2000;
        // Split birth date into day, month, year respectively
        int year = (int) (birthDateFull % 1E+4);
        int month = (int) (birthDateFull / 1E+4 % 100);
        int day = (int) (birthDateFull / 1E+6 % 100);
        // Initialize day and month in string format to comply with problem set revision (10/03/2021)
        String monthStr = String.format("%02d", month);
        String dayStr = String.format("%02d", day);
        // CASE 1: Check the month, make sure the day is following the 30/31 days rule
        // Check if it's 31-day month
        for (int i : oddMonth) {
            if (month == i) {
                if (day >= 1 && day <= 31) {
                    birthDateStr = dayStr + "-" + monthStr + "-" + year;
                }
            }
        }
        // Check if it's 30-day month
        for (int i : evenMonth) {
            if (month == i) {
                if (day >= 1 && day <= 30) {
                    birthDateStr = dayStr + "-" + monthStr + "-" + year;
                }
            }
        }
        // CASE 2: Check the month; if it's in February, check if it's leap year
        if (month == 2) {
            // If it's leap year, the maximum day is 29
            if (year % 4 == 0) {
                if (day >= 1 && day <= 29) {
                    birthDateStr = dayStr + "-" + monthStr + "-" + year;
                }
            }
            // If it's not leap year, the maximum day is 28
            else {
                if (day >= 1 && day <= 28) {
                    birthDateStr = dayStr + "-" + monthStr + "-" + year;
                }
            }
        }
        // Lastly, validate if the age is 15 years old and above
        // If under 15 years old, set birthDateStr into null value (therefore it's invalid)
        if (yearEntrance - year < 15) birthDateStr = null;
        // Return the birth date in string format (null if it's invalid birth date)
        return birthDateStr;
    }

    public static boolean codeValidation(long npm) {
        // Extract the first 13 digits of NPM and the NPM code
        long npmCheck = npm / 10;
        long npmCode = npm % 10;
        // PHASE 1: Multiply unique digits (according to rules)
        // Split npmCheck into digits with numberToArray method
        long[] digits = numberToArray(npmCheck);
        // Reset npmCheck into 0
        npmCheck = 0;
        // Do the rules according to the problem set
        for (int i = 0;i < 6;i++) {
            npmCheck += (digits[i] * digits[12-i]);
        }
        // Add the 7th digit to npmCheck
        npmCheck += digits[6];
        // PHASE 2: While npmCheck is more than one digit, keep adding each digits
        while (npmCheck > 9) {
            // Split npmCheck into digits again
            digits = numberToArray(npmCheck);
            // Reset npmCheck into 0
            npmCheck = 0;
            // Do the rules according to the problem set
            for (long i : digits) {
                npmCheck += i;
            }
        }
        // Check if npmCheck equals to npmCode; if it's not equal, it's invalid
        return npmCheck == npmCode;
    }

    public static long[] numberToArray(long num) {
        // Cast number into string type
        String str = String.valueOf(num);
        // Initialize temporary variables
        int len = str.length();
        long[] arr = new long[len];
        // Cast back each digit into long type
        for(int i = 0;i < len;i++){
            arr[i] = Integer.parseInt(Character.toString(str.charAt(i)));
        }
        // Return the array of long
        return arr;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // Validate NPM with validate method
            boolean status = validate(npm);
            // If valid, extract the information; else, print "invalid"
            if (status) {
                String output = extract(npm);
                System.out.println(output);
            }
            else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}