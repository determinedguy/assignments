package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    // Use counter to ease the calculation

    private int totalMataKuliah;

    Mahasiswa(String nama, long npm) {
        super("Mahasiswa", nama);
        this.npm = npm;
        this.tanggalLahir = extractTanggalLahir(npm);
        this.jurusan = extractJurusan(npm);
        this.totalMataKuliah = 0;
    }

    // Make getters

    public String getTanggalLahir() { return this.tanggalLahir; }

    public String getJurusan() { return this.jurusan; }

    public MataKuliah[] getDaftarMataKuliah() { return this.daftarMataKuliah; }

    public int getTotalMataKuliah() { return this.totalMataKuliah; }

    public void addMatkul(MataKuliah mataKuliah){
        // STEP 1: Check if the subject is taken already, print error status if the student took the subject already
        if (cekStatusMataKuliah(mataKuliah)) {
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah);
        }
        // STEP 2: Check if the subject is fully taken, print error status if the subject is fully filled
        else if (mataKuliah.getTotalMahasiswa() >= mataKuliah.getKapasitas()) {
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah);
        }
        // If there is no error, add the subject
        else {
            // Get the last empty position on daftarMataKuliah and add the subject
            this.daftarMataKuliah[this.totalMataKuliah++] = mataKuliah;
            // Add the student to the subject, too
            mataKuliah.addMahasiswa(this);
            // Print the message
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this, mataKuliah);
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        // Check if the subject is not taken already, print error status if the student hasn't taken the subject
        if (!(cekStatusMataKuliah(mataKuliah))) {
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah.toString());
        }
        // If there is no error, drop the subject
        else {
            // Make a temporary array which contains all of remaining subjects
            MataKuliah[] tempArr = new MataKuliah[10];
            int count = 0;
            for (int i = 0;i < this.totalMataKuliah;i++) {
                if (!(daftarMataKuliah[i].equals(mataKuliah))) {
                    tempArr[count] = daftarMataKuliah[i];
                    count++;
                }
            }
            //  Reference the new array to daftarMataKuliah
            this.daftarMataKuliah = tempArr;
            // Decrease the amount of courses enrolled
            this.totalMataKuliah--;
            // Drop the student from the subject, too
            mataKuliah.dropMahasiswa(this);
            // Print the message
            System.out.printf("%s berhasil drop mata kuliah %s\n", this, mataKuliah);
        }
    }

    public boolean cekStatusMataKuliah(MataKuliah mataKuliah) {
        // Check the list of subjects taken on the list, return true if the subject is in the list, else return false
        for (int i = 0;i < this.totalMataKuliah;i++) {
            if (this.daftarMataKuliah[i].equals(mataKuliah)) {
                return true;
            }
        }
        return false;
    }

    public static String extractTanggalLahir(long npm) {
        // Cast NPM to string, extract by substring, and cast back to integer
        int tanggalLahir = Integer.parseInt(Long.toString(npm).substring(4,6));
        int bulanLahir = Integer.parseInt(Long.toString(npm).substring(6,8));
        int tahunLahir = Integer.parseInt(Long.toString(npm).substring(8,12));
        // Use string formatting to return the string
        return String.format("%d-%d-%d", tanggalLahir, bulanLahir, tahunLahir);
    }

    public static String extractJurusan(long npm) {
        // Cast NPM to string and extract by substring
        String kodeJurusan = Long.toString(npm).substring(2,4);
        String jurusan;
        // Check the equality and determine the major
        if (kodeJurusan.equals("01")) {
            jurusan = "Ilmu Komputer";
        } else {
            jurusan = "Sistem Informasi";
        }
        // Return the major
        return jurusan;
    }

}