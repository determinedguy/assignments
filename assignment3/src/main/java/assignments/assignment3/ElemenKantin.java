package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {

    private Makanan[] daftarMakanan = new Makanan[10];

    // Using counter to ease the calculation

    private int totalDaftarMakanan;

    ElemenKantin(String nama) {
        super("ElemenKantin", nama);
    }

    // Make getter

    public Makanan[] getDaftarMakanan() { return this.daftarMakanan; }

    public int getTotalDaftarMakanan() { return this.totalDaftarMakanan; }

    public Makanan getMakanan(String namaMakanan) {
        // Check if the food is in the food list, return null if it isn't there
        for (int i = 0;i < totalDaftarMakanan;i++) {
            if (daftarMakanan[i].toString().equals(namaMakanan)) {
                return daftarMakanan[i];
            }
        }
        return null;
    }

    public boolean cekMakanan(Makanan makanan) {
        // Check if food has been added
        for (int i = 0;i < totalDaftarMakanan;i++) {
            if (daftarMakanan[i].toString().equals(makanan)) {
                return true;
            }
        }
        return false;
    }

    public void setMakanan(String nama, long harga) {
        // Initialize the food first
        Makanan makanan = new Makanan(nama, harga);
        // Check if the food has been added, return exclamation if the food has been added, else add the food
        if (cekMakanan(makanan)) {
            System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", makanan);
        } else {
            // Add the food to daftarMakanan and increase the totalDaftarMakanan
            this.daftarMakanan[this.totalDaftarMakanan++] = makanan;
            // Print the message
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this, makanan, makanan.getHarga());
        }
    }
}