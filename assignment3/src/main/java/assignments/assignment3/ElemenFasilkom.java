package assignments.assignment3;

abstract class ElemenFasilkom {

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    // Using counter to ease the calculation

    private int totalTelahMenyapa;

    // Making constructor

    ElemenFasilkom(String tipe, String nama) {
        this.tipe = tipe;
        this.nama = nama;
        // Initial value of friendship is 0
        this.friendship = 0;
    }

    // Making setter and getter

    public String getTipe() { return this.tipe; }

    public int getFriendship() { return this.friendship; }

    public void setFriendship(int newFriendship) {
        // Range of friendship is 0-100
        if (newFriendship >= 100) {
            this.friendship = 100;
        } else if (newFriendship <= 0) {
            this.friendship = 0;
        } else {
            this.friendship = newFriendship;
        }
    }

    public int getTotalTelahMenyapa() { return this.totalTelahMenyapa; }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        // Check if the person and the other person has greeted each other, return exclamation if they have greeted
        if (cekSalingMenyapa(elemenFasilkom)) {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this, elemenFasilkom);
        }
        else {
            // Add the person to the telahMenyapa and increase the totalTelahMenyapa
            this.telahMenyapa[this.totalTelahMenyapa++] = elemenFasilkom;
            // Add this person to the telahMenyapa and increase the totalTelahMenyapa
            elemenFasilkom.telahMenyapa[elemenFasilkom.totalTelahMenyapa++] = this;
            // Print the message
            System.out.printf("%s menyapa dengan %s\n", this, elemenFasilkom);
            // Check if the person is Dosen and the other is Mahasiswa (or vice versa),
            // pass the people (with down casting) to dosenMenyapaMahasiswa if fulfilled (to calculate friendship)
            if (this.tipe.equals("Dosen") && elemenFasilkom.tipe.equals("Mahasiswa")) {
                dosenMenyapaMahasiswa((Dosen) this, (Mahasiswa) elemenFasilkom);
            } else if (this.tipe.equals("Mahasiswa") && elemenFasilkom.tipe.equals("Dosen")) {
                dosenMenyapaMahasiswa((Dosen) elemenFasilkom, (Mahasiswa) this);
            }
        }
    }

    public void resetMenyapa() {
        // Reset telahMenyapa and totalTelahMenyapa
        this.telahMenyapa = new ElemenFasilkom[100];
        this.totalTelahMenyapa = 0;
    }

    public boolean cekSalingMenyapa(ElemenFasilkom elemenFasilkom) {
        // Check if the person and the other person has greeted each other
        for (int i = 0;i < this.totalTelahMenyapa;i++) {
            if (this.telahMenyapa[i].equals(elemenFasilkom)) {
                return true;
            }
        }
        return false;
    }

    public static void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        // Check if the food is sold by the seller, print the exclamation if it's not sold, else proceed
        if (!cekMakananDijual(penjual, namaMakanan)) {
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
        } else {
            // Increase the friendships by one
            pembeli.setFriendship(pembeli.getFriendship()+1);
            penjual.setFriendship(penjual.getFriendship()+1);
            // Use down casting to get the food
            Makanan makanan = ((ElemenKantin) penjual).getMakanan(namaMakanan);
            System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, makanan, makanan.getHarga());
        }
    }

    public static boolean cekMakananDijual(ElemenFasilkom penjual, String namaMakanan) {
        // Check if the food is sold by the seller, return true if it's in food list, else return false
        // Use down casting to get the food list and amount
        Makanan[] daftarMakanan = ((ElemenKantin) penjual).getDaftarMakanan();
        int jumlahMakanan = ((ElemenKantin) penjual).getTotalDaftarMakanan();
        for (int i = 0;i < jumlahMakanan;i++) {
            if (daftarMakanan[i].toString().equals(namaMakanan)) {
                return true;
            }
        }
        return false;
    }

    public static void dosenMenyapaMahasiswa(Dosen dosen, Mahasiswa mahasiswa) {
        // Assuming that this method is "from the lecturer's point of view"
        // Get the subject which the lecturer teaches
        MataKuliah mataKuliah = dosen.getMataKuliah();
        // Check if the subject is not null, proceed if fulfilled
        if (mataKuliah != null) {
            // Get the student's list from the subject
            Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();
            int totalMahasiswa = mataKuliah.getTotalMahasiswa();
            // Check if the student is in the list,
            // increase the friendship value by 2 if fulfilled and break
            for (int i = 0; i < totalMahasiswa; i++) {
                if (daftarMahasiswa[i].equals(mahasiswa)) {
                    dosen.setFriendship(dosen.getFriendship()+2);
                    mahasiswa.setFriendship(mahasiswa.getFriendship()+2);
                    break;
                }
            }
        }
    }

    public String toString() {
        // Return the name of the person
        return this.nama;
    }
}