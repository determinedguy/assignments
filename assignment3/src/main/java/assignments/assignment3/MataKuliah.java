package assignments.assignment3;

class MataKuliah {

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    // Use counter to ease the calculation

    private int totalMahasiswa;

    MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
        this.totalMahasiswa = 0;
    }

    // Make getter and setter

    public int getKapasitas() { return this.kapasitas; }

    public int getTotalMahasiswa() {
        return this.totalMahasiswa;
    }

    public Dosen getDosen() { return this.dosen; }

    public Mahasiswa[] getDaftarMahasiswa() { return this.daftarMahasiswa; }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Check the last empty position of daftarMahasiswa and add the student to the position
        this.daftarMahasiswa[this.totalMahasiswa++] = mahasiswa;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // Make a temporary array which contains all of remaining students
        Mahasiswa[] tempArr = new Mahasiswa[this.kapasitas];
        int count = 0;
        for (Mahasiswa tempMahasiswa : this.daftarMahasiswa) {
            if (tempMahasiswa != null && !(tempMahasiswa.equals(mahasiswa))) {
                tempArr[count] = tempMahasiswa;
                count++;
            }
        }
        //  Reference the new array to daftarMahasiswa
        this.daftarMahasiswa = tempArr;
        // Decrease the amount of students enrolling the course
        this.totalMahasiswa--;
    }

    void addDosen(Dosen dosen) {
        // Assign the lecturer from the subject
        this.dosen = dosen;
    }

    void dropDosen() {
        // Drop the lecturer from the subject ("reset" to null)
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}