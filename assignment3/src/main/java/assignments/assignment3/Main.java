package assignments.assignment3;

import java.util.*;

public class Main {

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0;

    private static void addMahasiswa(String nama, long npm) {
        // Initialize a new student
        Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
        // Add to daftarElemenFasilkom and increase the counter
        daftarElemenFasilkom[totalElemenFasilkom++] = mahasiswa;
        // Print the message
        System.out.printf("%s berhasil ditambahkan\n", mahasiswa);
    }

    private static void addDosen(String nama) {
        // Initialize a new lecturer
        Dosen dosen = new Dosen(nama);
        // Add to daftarElemenFasilkom and increase the counter
        daftarElemenFasilkom[totalElemenFasilkom++] = dosen;
        // Print the message
        System.out.printf("%s berhasil ditambahkan\n", dosen);
    }

    private static void addElemenKantin(String nama) {
        // Initialize a new person
        ElemenKantin elemenKantin = new ElemenKantin(nama);
        // Add to daftarElemenFasilkom and increase the counter
        daftarElemenFasilkom[totalElemenFasilkom++] = elemenKantin;
        // Print the message
        System.out.printf("%s berhasil ditambahkan\n", elemenKantin);
    }

    // Adding getter to ease the searching of the person and subject
    private static ElemenFasilkom getElemenFasilkom(String nama) {
        // Check if the person is in the list, return null if they aren't there
        // Use indexing (not foreach) to avoid null value
        for (int i = 0;i < totalElemenFasilkom;i++) {
            if (daftarElemenFasilkom[i].toString().equals(nama)) {
                return daftarElemenFasilkom[i];
            }
        }
        return null;
    }

    private static MataKuliah getMataKuliah(String nama) {
        // Check if the subject is in the list, return null if they aren't there
        // Use indexing (not foreach) to avoid null value
        for (int i = 0;i < totalMataKuliah;i++) {
            if (daftarMataKuliah[i].toString().equals((nama))) {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    private static void menyapa(String objek1, String objek2) {
        // Initialize them in ElemenFasilkom object
        ElemenFasilkom orang1 = getElemenFasilkom(objek1);
        ElemenFasilkom orang2 = getElemenFasilkom(objek2);
        // Check if orang1 and orang2 are the same object, reject if they're same
        // Else, proceed by adding the menyapa mechanism
        if (orang1.equals(orang2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            // The method automatically adds each other
            orang1.menyapa(orang2);
        }
    }

    private static void addMakanan(String objek, String namaMakanan, long harga) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check the person "type", throw exclamation part if they're not ElemenKantin
        // Else, proceed by adding the food
        if (!orang.getTipe().equals("ElemenKantin")) {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", orang);
        } else {
            // Use down casting to get the adder method
            ((ElemenKantin) orang).setMakanan(namaMakanan, harga);
        }
    }

    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        // Initialize them in ElemenFasilkom object
        ElemenFasilkom orang1 = getElemenFasilkom(objek1);
        ElemenFasilkom orang2 = getElemenFasilkom(objek2);
        // STEP 1: Check if orang2 is an ElemenKantin, throw exclamation if they aren't
        // STEP 2: Check if orang1 is the same person (object) as orang2, throw exclamation if they are
        // STEP 3: Else, proceed by the method from ElemenFasilkom class
        if (!orang2.getTipe().equals("ElemenKantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        } else if (orang1.equals(orang2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else {
            ElemenFasilkom.membeliMakanan(orang1, orang2, namaMakanan);
        }
    }

    private static void createMatkul(String nama, int kapasitas) {
        // Initialize a new subject
        MataKuliah mataKuliah = new MataKuliah(nama, kapasitas);
        // Add to daftarMataKuliah and increase the counter
        daftarMataKuliah[totalMataKuliah++] = mataKuliah;
        // Print the message
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", mataKuliah, mataKuliah.getKapasitas());
    }

    private static void addMatkul(String objek, String namaMataKuliah) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check if the person is a student, throw exclamation if they aren't
        // Else, proceed by adding the subject
        if (!orang.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        } else {
            // Initialize the subject in MataKuliah object
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            // Use addMatkul method from Mahasiswa class (by down casting)
            ((Mahasiswa) orang).addMatkul(mataKuliah);
        }
    }

    private static void dropMatkul(String objek, String namaMataKuliah) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check if the person is a student, throw exclamation if they aren't
        // Else, proceed by removing the subject
        if (!orang.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        } else {
            // Initialize the subject in MataKuliah object
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            // Use dropMatkul method from Mahasiswa class (by down casting)
            ((Mahasiswa) orang).dropMatkul(mataKuliah);
        }
    }

    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check if the person is a lecturer, throw exclamation if they aren't
        // Else, proceed by adding the subject
        if (!orang.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        } else {
            // Initialize the subject in MataKuliah object
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            // Use mengajarMataKuliah method from Dosen class (by down casting)
            ((Dosen) orang).mengajarMataKuliah(mataKuliah);
        }
    }

    private static void berhentiMengajar(String objek) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check if the person is a lecturer, throw exclamation if they aren't
        // Else, proceed by removing the subject
        if (!orang.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        } else {
            // Use dropMataKuliah method from Dosen class (by down casting)
            ((Dosen) orang).dropMataKuliah();
        }
    }

    private static void ringkasanMahasiswa(String objek) {
        // Initialize the person in ElemenFasilkom object
        ElemenFasilkom orang = getElemenFasilkom(objek);
        // Check if the person is a student, throw exclamation if they aren't
        // Else, proceed by making the summary
        if (!orang.getTipe().equals("Mahasiswa")) {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", orang);
        } else {
            // Down cast first to ease the code
            Mahasiswa mahasiswa = (Mahasiswa) orang;
            // Print the information
            System.out.printf("Nama: %s\n", mahasiswa);
            System.out.printf("Tanggal lahir: %s\n", mahasiswa.getTanggalLahir());
            System.out.printf("Jurusan: %s\n", mahasiswa.getJurusan());
            System.out.println("Daftar Mata Kuliah:");
            // Check if the student has taken (at least one) subject(s), return exclamation if they haven't
            // Else, print the subject(s) per line
            int totalMataKuliah = mahasiswa.getTotalMataKuliah();
            if (totalMataKuliah == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                for (int i = 0; i < totalMataKuliah; i++) {
                    System.out.printf("%d. %s\n", i + 1, mahasiswa.getDaftarMataKuliah()[i]);
                }
            }
        }
    }

    private static void ringkasanMataKuliah(String namaMataKuliah) {
        // Initialize the subject in MataKuliah object
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        // Print the information
        int totalMahasiswa = mataKuliah.getTotalMahasiswa();
        System.out.printf("Nama mata kuliah: %s\n", mataKuliah);
        System.out.printf("Jumlah mahasiswa: %s\n", totalMahasiswa);
        System.out.printf("Kapasitas: %s\n", mataKuliah.getKapasitas());
        System.out.print("Dosen pengajar: ");
        // If there is a lecturer who teaches the class, print their name, else print exclamation
        Dosen dosen = mataKuliah.getDosen();
        if (dosen == null) {
            System.out.println("Belum ada");
        } else {
            System.out.println(dosen);
        }
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        // If there is at least a student who takes the class, print their name per line, else print exclamation
        if (totalMahasiswa == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            for (int i = 0; i < totalMahasiswa; i++) {
                System.out.printf("%d. %s\n", i + 1, mataKuliah.getDaftarMahasiswa()[i]);
            }
        }
    }

    private static void nextDay() {
        // Use foreach loop to iterate each person
        for (int i = 0;i < totalElemenFasilkom;i++) {
            ElemenFasilkom orang = daftarElemenFasilkom[i];
            // STEP 1: Calculate the friendliness and store it to friendship
            int totalTelahMenyapa = orang.getTotalTelahMenyapa();
            int friendship = orang.getFriendship();
            // If the person greets more than half of people in total (excluding themselves), add 10
            // Else, subtract by 5
            if (totalTelahMenyapa >= Math.round(((totalElemenFasilkom - 1) / 2.0))) {
                friendship += 10;
            } else {
                friendship -= 5;
            }
            // Set the friendship to the person
            orang.setFriendship(friendship);
            // STEP 2: Reset the telahMenyapa array
            orang.resetMenyapa();
        }
        // STEP 3: Print the message
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        // STEP 4: Call friendshipRanking method
        friendshipRanking();
    }

    private static void friendshipRanking() {
        // Initialize ArrayList for storing the rank
        ArrayList<String> rank = new ArrayList<>();
        // Sort daftarElemenFasilkom based on friendship and name, use counting sort method
        // Reference: https://www.geeksforgeeks.org/join-two-arraylists-in-java/
        for (int i = 100;i >= 0;i--) {
            // Initialize temporary ArrayList
            ArrayList<String> temp = new ArrayList<>();
            // Iterate daftarElemenFasilkom and check if a person (or people) is in the range, add if any
            for (int j = 0;j < totalElemenFasilkom;j++) {
                ElemenFasilkom orang = daftarElemenFasilkom[j];
                if (orang.getFriendship() == i) {
                    temp.add(orang.toString());
                }
            }
            // Sort temp ArrayList by name (in case there are more than a person in the range)
            Collections.sort(temp);
            // Append the temporary ArrayList to rank
            rank.addAll(temp);
        }
        // Print the rank
        int count = 1;
        for (String nama : rank) {
            // Get the friendship
            int friendship = getElemenFasilkom(nama).getFriendship();
            System.out.printf("%d. %s(%d)\n", count++, nama, friendship);
        }
    }

    private static void programEnd() {
        // Print the message
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        // Call friendshipRanking method
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}