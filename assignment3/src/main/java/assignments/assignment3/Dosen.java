package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    Dosen(String nama) {
        super("Dosen", nama);
    }

    // Make getter

    public MataKuliah getMataKuliah() { return this.mataKuliah; }

    void mengajarMataKuliah(MataKuliah mataKuliah) {
        // Check if the lecturer has any subject to teach, print exclamation if has
        // Also check if the subject has the lecturer, print exclamation if has
        // Else, assign the subject (and vice versa) and print the message
        if (this.mataKuliah != null) {
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this, this.mataKuliah);
        } else if (mataKuliah.getDosen() != null) {
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
        } else {
            this.mataKuliah = mataKuliah;
            this.mataKuliah.addDosen(this);
            System.out.printf("%s mengajar mata kuliah %s\n", this, mataKuliah);
        }
    }

    void dropMataKuliah() {
        // Check if the lecturer has any subject to teach, print exclamation if hasn't
        // Else, drop the lecturer from the subject ("reset" to null) and vice versa
        if (this.mataKuliah == null) {
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this);
        } else {
            System.out.printf("%s berhenti mengajar %s\n", this, this.mataKuliah);
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }
}